#!/bin/sh

set -e

# -----------------------------------------

printf 'Retrieving new key...\n'
new_key="$(
	wget -q -O - 'https://www.makemkv.com/forum/viewtopic.php?f=5&t=1053' | \
		grep -Po '(?<=<code>)T[^<>]+'
)"
if ! [ "$(expr length "${new_key}")" -eq 68 ] || ! [ "$(expr substr "${new_key}" 1 2)" = 'T-' ]
then
	printf 'Error: Failed to retrieve key from a webserver.\n'
	exit 1
fi
printf 'The latest beta key is: `%s`\n' "${new_key}"

printf '\n'
# -----------------------------------------

printf 'Updating config file...\n'

cfg_dir_path="$(realpath ~/.MakeMKV)"
if ! [ -d "${cfg_dir_path}" ]
then
	mkdir -v -p "${cfg_dir_path}"
fi
if ! [ -w "${cfg_dir_path}" ]
then
	printf 'Error: `%s` is not writeable!\n' "${cfg_dir_path}"
	exit 1
fi

cfg_f_name="settings.conf"
cfg_f="${cfg_dir_path}/${cfg_f_name}"

# delete old value if exists
if [ -f "${cfg_f}" ]
then
	sed -r -e '/^ *app_Key/d' -i -- "${cfg_f}"
fi

printf 'app_Key = "%s"\n' "${new_key}" >>"${cfg_f}"
if [ $? -ne 0 ]
then
	printf 'Error: Appending to the config file failed.\n'
	exit 1
else
	printf 'OK\n'
fi

printf '\n'
# -----------------------------------------
